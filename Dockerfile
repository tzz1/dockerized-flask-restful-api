FROM python:3.6
LABEL maintainer="tzz@lifelogs.com"

# # Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# # Or delete entirely if not needed.
# RUN apt-get update \
#     && apt-get install -y --no-install-recommends \
#         postgresql-client \
#     && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# at BUILD time we can override the port...
ARG PORT=5000
# for the EXPOSE...
EXPOSE ${PORT}

# but at run time we can override the FLASK_PORT without rebuilding
ENV FLASK_APP=api.py
ENV FLASK_ENV=development
ENV FLASK_PORT=${PORT}

CMD flask run -p ${FLASK_PORT}
