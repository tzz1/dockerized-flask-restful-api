TAG:=$(shell git describe --tags)

run:
	python test.py # this would be a CI step
	docker build -t mytester:$(TAG) -f Dockerfile .
	docker tag mytester:$(TAG) mytester:latest
	docker-compose up -d
	docker-compose ps
	# run an external test
	docker-compose exec mytester curl localhost:5000/comments | jq .
