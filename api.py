import uuid
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)

# from https://flask-restful.readthedocs.io/en/latest/quickstart.html
# The data store is a list of key-value entries similar to DB rows
# This code is not thread-safe
COMMENTS = [
    {'id': 'uno', 'text': 'one', 'likes': 0},
    {'id': 'dos', 'text': 'two two', 'likes': 0},
    {'id': 'tres', 'text': 'three three three', 'likes': 0},
]

### The below are the APIs that talk directly to the storage backend. Usually
### they will be hidden behind a class or other callable interface.

def return_comment_or_abort(entry_id):
    """ Returns the comment or *aborts* if it's missing
    """

    for entry in COMMENTS:
        if entry_id == entry['id']:
            return entry

    abort(404, message="comment {} doesn't exist".format(entry_id))

def return_comment_or_none(entry_id):
    """ Returns the comment or None
    """

    for entry in COMMENTS:
        if entry_id == entry['id']:
            return entry

    return None

def create_comment(entry_id, text, likes):
    """Creates the comment. If it can't create, it will *abort*.
    Does not check arguments for validity.

    It will pick a random new entry_id if the given one is None, and will
    *abort* if the random ID was in the list already.

    """

    # Yes, the parser will only pass integers, but it's better to add an
    # extra conversion here to be 100% sure only integers will get into
    # storage.
    if likes is None:
        likes = 0

    entry = {'text': text, 'likes': likes}
    if entry_id is None:
        # this will normally be handled by the storage layer / database
        entry_id = str(uuid.uuid4())

        if return_comment_or_none(entry_id) is not None:
            abort(404, message="autogeneration of entry ID {} collided".format(entry_id))
    # else, we're given an existing ID, abort
    if return_comment_or_none(entry_id) is not None:
        abort(404, message="existing entry ID {} can't be modified".format(entry_id))

    # add new comment
    entry['id'] = entry_id
    COMMENTS.append(entry)
    return return_comment_or_abort(entry_id)

def overwrite_comment(comment):
    """Overwrites the comment. If it can't find it, it will *abort*. Does not check
    arguments for validity.

    This code will *replace* the previous entry if anything changes (especially
    note this for likes). This is very suboptimal.

    """
    # There must be an existing entry. Delete it.
    entry = return_comment_or_abort(comment['id'])
    delete_comment(comment['id'])
    COMMENTS.append(comment)
    return return_comment_or_abort(comment['id'])

def delete_comment(entry_id):
    """ Deletes the comment or *aborts* if it's missing
    """
    global COMMENTS # we'll be modifying a global variable

    if return_comment_or_none(entry_id) is None:
        abort(404, message="comment {} doesn't exist".format(entry_id))

    COMMENTS = [entry for entry in COMMENTS if entry['id'] != entry_id]
    return

### End of storage backend

parser = reqparse.RequestParser()
parser.add_argument('text', help='Comment text')
parser.add_argument('likes', type=int, help='How many likes the comment has')
parser.add_argument('delta_likes', type=int, help='Add this many likes to the comment')

class CommentManager(Resource):
    """ Show/delete/overwrite/upload a single entry
    """

    def get(self, entry_id):
        """ Returns the comment for a specific entry ID, or aborts
        """
        return return_comment_or_abort(entry_id)

    def delete(self, entry_id):
        """ Deletes the comment for a specific entry ID, or aborts
        """
        entry = delete_comment(entry_id)
        return '', 204

    def put(self, entry_id):
        args = parser.parse_args()

        comment = return_comment_or_abort(entry_id)

        new_text = args.get('text')
        if new_text is not None:
            comment['text'] = new_text

        new_likes = args.get('likes')
        if new_likes is not None:
            comment['likes'] = int(new_likes)

        delta_likes = args.get('delta_likes')
        if delta_likes is not None:
            comment['likes'] += int(delta_likes)

        comment = overwrite_comment(comment)
        return comment, 201

# shows a list of all comments, and lets you POST to add new comments
class CommentLister(Resource):
    def get(self):
        return COMMENTS

    def post(self):
        args = parser.parse_args()
        comment = create_comment(None,
                                 args.get('text', 'no text provided'),
                                 args.get('likes', 0))

        return comment, 201

##
## Actually setup the Api resource routing here
##
api.add_resource(CommentLister, '/comments')
api.add_resource(CommentManager, '/comments/<entry_id>')


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
