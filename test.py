#!/usr/bin/python

# original from https://scotch.io/tutorials/build-a-restful-api-with-flask-the-tdd-way
import unittest
import os
import json
from api import app

class CommentTestCase(unittest.TestCase):
    """This class represents the comment test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = app
        self.client = self.app.test_client
        self.comment = {'text': 'Go to Borabora for vacation'}

        # UNUSED # binds the app to the current context
        # with self.app.app_context():
        #     # create all tables
        #     db.create_all()

    def test_comment_creation(self):
        """Test API can create a comment (POST request)"""
        res = self.client().post('/comments', data=self.comment)
        self.assertEqual(res.status_code, 201)
        self.assertIn('Go to Borabora', str(res.data))

    def test_api_can_get_all_comments(self):
        """Test API can get a comment (GET request)."""
        res = self.client().post('/comments', data=self.comment)
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/comments')
        self.assertEqual(res.status_code, 200)
        self.assertIn('Go to Borabora', str(res.data))

    def test_api_can_get_comment_by_id(self):
        """Test API can get a single comment by using it's id."""
        rv = self.client().post('/comments', data=self.comment)
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))
        result = self.client().get(
            '/comments/{}'.format(result_in_json['id']))
        self.assertEqual(result.status_code, 200)
        self.assertIn('Go to Borabora', str(result.data))

    def test_comment_can_be_edited(self):
        """Test API can edit an existing comment. (PUT request)"""
        rv = self.client().post(
            '/comments',
            data={'text': 'Eat, pray and love'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))
        rv = self.client().put(
            '/comments/{}'.format(result_in_json['id']),
            data={
                "text": "Dont just eat, but also pray and love :-)"
            })
        self.assertEqual(rv.status_code, 201)
        results = self.client().get('/comments/{}'.format(result_in_json['id']))
        self.assertIn('Dont just eat', str(results.data))

    def test_comment_can_be_liked(self):
        """Test API can add/remove likes an existing comment. (PUT request)"""
        rv = self.client().post(
            '/comments',
            data={'text': 'Eat, pray and love'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        target_id = result_in_json['id']
        current = 0
        for delta in [0, 1,100,-1,-200]:
            rv = self.client().put(
                '/comments/{}'.format(target_id),
                data={
                "delta_likes": delta
            })
            self.assertEqual(rv.status_code, 201)

            current += delta # adjust the expected

            results = self.client().get('/comments/{}'.format(target_id))
            result_in_json = json.loads(results.data.decode('utf-8').replace("'", "\""))
            self.assertEqual(current, result_in_json['likes'])

    def test_comment_deletion(self):
        """Test API can delete an existing comment. (DELETE request)."""
        rv = self.client().post(
            '/comments',
            data={'name': 'Eat, pray and love'})
        self.assertEqual(rv.status_code, 201)

        res = self.client().get('/comments')
        self.assertEqual(res.status_code, 200)
        result_in_json = json.loads(res.data.decode('utf-8').replace("'", "\""))

        for entry in result_in_json:
            res = self.client().delete('/comments/{}'.format(entry['id']))
            self.assertEqual(res.status_code, 204)
            # Test to see if it exists, should return a 404
            result = self.client().get('/comments/{}'.format(entry['id']))
            self.assertEqual(result.status_code, 404)

    def tearDown(self):
        """UNUSED teardown all initialized variables."""
        pass
        # with self.app.app_context():
        #     # drop all tables
        #     db.session.remove()
        #     db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
