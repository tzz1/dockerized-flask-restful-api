# dockerized-flask-restful-api

## How to use this

Just run `make`. The output will look like this:

```console
% make

python test.py # this would be a CI step
......
----------------------------------------------------------------------
Ran 6 tests in 0.073s

OK
docker build -t mytester:v0.0.1-1-gd78922a -f Dockerfile .
Sending build context to Docker daemon  162.3kB
...
Successfully built e245fff37c07
Successfully tagged mytester:v0.0.1-1-gd78922a

docker tag mytester:v0.0.1-1-gd78922a mytester:latest

docker-compose up -d
Recreating dockerized-flask-restful-api_mytester_1 ... done
docker-compose ps
                 Name                                Command               State           Ports         
---------------------------------------------------------------------------------------------------------
dockerized-flask-restful-api_mytester_1   /bin/sh -c flask run -p ${ ...   Up      0.0.0.0:8080->5000/tcp
```

This is hacky - it should live in a CI setup like Jenkins or GitLab etc.

## Prep
I added the MIT license and a Python template .gitignore.

## Start
I then used the built-in GitLab Dockerfile and CI/CD templates as a starting
point. The Django template seemed good for my needs because it had a PostgreSQL
instance. But I didn't pursue building Docker images with docker-in-docker on
GitLab because it was too complex for the scope of the coding exercise, and
instead did the builds from the command line with docker-compose.yml. So the
GitLab support for staged builds, Docker tagging and uploading, database links,
etc etc was not used.

## API and testing

The API code came
from https://flask-restful.readthedocs.io/en/latest/quickstart.html and I
started the test code based
on https://scotch.io/tutorials/build-a-restful-api-with-flask-the-tdd-way

The Makefile is a quick dirty way to get the Docker image tag and assign it to
the build container, and then then run `test.py` before proceeding with the
image build. The image is tagged as "the latest tag plus a suffix" as per `git
describe --tags`, so a unique Docker image tag is generate for every commit.
This is a lot easier in Jenkins or GitLab with build stages and captured build
artifacts.

The test code exercises all the behavior using local unit testing. For a full
test I would automate externally against the running container. This is a lot
easier in Jenkins or GitLab.

The `PORT` build-time argument and the `FLASK_ENV` `FLASK_PORT` run-time
environment variables are available for customization. The `FLASK_PORT` is a
possible security exploit because we pass it directly down without validation.

I did not have time to put up a proxy in front of the app, sorry. The
`docker-compose.yml` is the absolute minimum to get this going. I did not use
the docker-compose built-in image build tools because they are not as good as
Jenkins or GitLab.

### API endpoints

This will be better done in something like Swagger... very quick RESTful API here:

#### GET /comments

Do a GET against /comments to list all the comments.

Arguments: none (TODO: proper searching of course)

Return: array of comment objects like `[ { id: UUID-OF-COMMENT, text: TEXT, likes: LIKES }, ... ]`
Success return code: 200


#### POST /comments

Do a POST against /comments to create a new comment. The new comment will be returned.

Arguments:

* `text`: comment text
* `likes`: initial likes, default is 0

Return: `{ id: ID-OF-NEW-COMMENT, text: GIVEN-TEXT, likes: GIVEN-LIKES }`
Success return code: 201

#### GET /comments/ID-OF-COMMENT

Do a GET against `/comments/ID-OF-COMMENT` to retrieve a comment.

Arguments: none

Return: `{ id: UUID-OF-COMMENT, text: TEXT, likes: LIKES }`
Success return code: 200

#### DELETE /comments/ID-OF-COMMENT

Do a DELETE against `/comments/ID-OF-COMMENT` to delete a comment.

Arguments: none

Return: `{ id: UUID-OF-COMMENT, text: TEXT, likes: LIKES }`
Success return code: 204

#### PUT /comments/ID-OF-COMMENT

Do a PUT against `/comments/ID-OF-COMMENT` to modify a comment.

Arguments (any of the above can be specified):

* `text`: comment text
* `likes`: absolute number of likes
* `delta_likes`: delta number of likes, can be positive or negative

If both `likes` and `delta_likes` are specified, the delta will be applied to
the given number.

Return: `{ id: UUID-OF-COMMENT, text: TEXT, likes: LIKES }`
Success return code: 201

## Cautions

The code **IS NOT** thread-safe. Implementing it correctly would rely on a real
storage layer.

The backend storage is just an in-memory list of dicts, built in 30 minutes.
It's completely thread unsafe, a total hack, and I would not use it for anything
but a demo of Flask and some Python fun.

## Next steps

As a next step I would migrate the backend storage to Redis and front it with a proper web server.
